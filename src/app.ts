import * as path from 'path';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import * as HttpStatus from 'http-status-codes';
import * as express from 'express';

const signale = require('signale');
const rateLimit = require("express-rate-limit");
const helmet = require('helmet');
const cors = require('cors');

import { Router, Request, Response, NextFunction } from 'express';

// Import routings
import indexRoute from './routes/index';

const app: express.Application = express();

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100 // limit each IP to 100 requests per windowMs
});


app.use((req: Request, res: Response, next: NextFunction) => {
  req.logger = signale;
  next();
});

app.use(limiter);
app.use(helmet({ hidePoweredBy: { setTo: 'PHP 4.2.0' } }));
app.use(cors());

// app.use(logger('dev'));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.get('/favicon.ico', (req: Request, res: Response) => res.status(204));

app.use('/', indexRoute);

//error handlers

if (process.env.NODE_ENV === 'development') {
  app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    req.logger.error(err.stack);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      error: {
        ok: false,
        code: HttpStatus.INTERNAL_SERVER_ERROR,
        error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR)
      }
    });
  });
}

app.use((req: Request, res: Response, next: NextFunction) => {
  res.status(HttpStatus.NOT_FOUND).json({
    error: {
      ok: false,
      code: HttpStatus.NOT_FOUND,
      error: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
    }
  });
});

export default app;
