// import Knex = require('knex');
import { Signale } from 'signale';

declare global {
  namespace Express {
    export interface Request {
      // db: Knex;
      decoded: any;
      logger: Signale
    }
  }
}
